function foobar (times) {
    var str = '';
    for (let i = 0; i < times; i++) {
        str += 'foo';
    }

    return str + 'bar';
}

function suite() {
    var tests = [];

    return {
        test : function (label, method) {
            tests.push({ label : label, method : method });
        },

        run : function () {
            var results = [];
            for (let i = 0; i < tests.length; i++) {
                let result = true;

                try {
                    tests[i].method();
                } catch (err) {
                    process.exitCode = 1;
                    result = err;
                }

                results.push({
                    label : tests[i].label,
                    result : result
                });
            }

            results.forEach(function (result) {
                console.log(`${result.label}: `, result.result === true ? 'Passed' : 'Failed');

                if (result.result !== true) {
                    console.log('Error: ', result.result.message);
                }
            });

            console.log(`Tests ${ process.exitCode ? 'failed' : 'passed' }!`);
        }
    };
}

var assert = require('assert');
var suite = suite();

suite.test('foobar', function () {
    assert.strictEqual(foobar(3), 'foofoofoobar');
});

suite.test('foobar2', function () {
    assert.strictEqual(foobar(4), 'foofoofoofoobar');
});

suite.run();
