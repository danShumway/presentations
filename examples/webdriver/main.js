var http = require('http');

//Spawn child processes in Node
var spawn = (function () {
    var spawn = require('child_process').spawn;
    return function (cmd, props) {
        var proc = spawn(cmd, props || [], { detached: true });

        proc.stdout.on('data', (data) => { console.log('out:', data.toString()); });
        proc.stderr.on('data', (data) => { console.log('err:', data.toString()); });

        return proc;
    };
}());


//Yes, yes, I know.
var request = require('request');

//Launch webdriver
var webdriver = spawn('/home/danshumway/geckodriver');
var echo = spawn('http-server', ['~/presentations']);

(async function () {
    await new Promise(function (res) {
        echo.stdout.on('data', () => res());
    });

    //Start a new webdriver session
    var session = await new Promise(function (res, rej) {
        request.post({
            url: 'http://localhost:4444/session',
            json: true,
            body : {},
        },function (error, result, body) {
            res(body);
        });
    });

    console.log(session);

    var address = await new Promise(function (res, rej) {
        request.post({
            url: `http://localhost:4444/session/${session.value.sessionId}/url`,
            json : true,
            body : { url: 'https://google.com'},
        }, function (err, result, body) {
            res(body);
        });
    });

    var execute = await new Promise(function (res, rej) {
        request.post({
            url: `http://localhost:4444/session/${session.value.sessionId}/execute/sync`,
            json: true,
            body : { script: `window.foo = 'hello barcamp'; return window.location;`, args: [] },
        }, function (err, result, body) {
            res(body);
        });
    });

    console.log('script result:', execute);

    await new Promise(function (res) { setTimeout(res, 1000000); });

    //Kill everything
    process.kill(-webdriver.pid);
    process.kill(-echo.pid);

    //clean up 
}());
