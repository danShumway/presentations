#+REVEAL_PLUGINS: (highlight)
#+EXPORT_FILE_NAME: public/abstractions.html
#+OPTIONS: reveal_title_slide:nil
#+OPTIONS: toc:nil 
#+OPTIONS: num:nil

* Abstract Your Code
and other bad advice I learned in school.

* Daniel Shumway

https://danshumway.com

Twitter: @DanielShumway

** Reset Hard

A time traveling puzzle game about learning to do impossible things.

** Software development at Oracle 

Roughly 3 years.

** Rochester Institute of Technology

BS in Game Design and Development

* You have to spend money to save money.

** /Used/ to be an insightful thing to say

* There's a lot of advice that you're going to get in formal education that /used/ to be insightful.

** Don't use global variables

** Break apart large functions

** Never use a public variable

** Prefer high-level abstractions

* Why abstract code?

- Hides messy details
- Forces consistency
- Solidifies APIs and paradigms
- Separates intent from implementation

** But there are some bad side effects.

- Hides messy details
- Forces consistency
- Solidifies APIs and paradigms
- Separates intent from implementation


* Abstractions don't make code simpler.

** You start with an implementation.

#+BEGIN_SRC javascript
var title = document.querySelector('#title');
title.textContent = 'hello world';
#+END_SRC

** You add an abstraction on top of it

#+BEGIN_SRC javascript
function Title () {
    this._element = document.querySelector('#title');
}

Title.prototype = {
    constructor: Title,
    setText : function (text) {
        this._element.text = text;
    }
};

var title = new Title();
title.setText('hello world');
#+END_SRC

** But implementation never goes away.

You don't get to forget about it.


** 100% of the time, abstractions make your code more complicated.

* So why would anyone abstract anything?

** Sometimes you have to make things more complicated to make them simpler.

** Sometimes you have to have a complicated framework so that your code will be simpler to write.

** Sometimes you have to build a complicated API up front so that your code will be simpler to maintain.

** Sometimes you have to spend money to save money.

* Bull Crap

** The best way to save money is to /save money/.

** The best way to reduce complexity is to /do fewer complicated things./

** Saving money by spending money is not normal. It's the /exception/.

* Why do we do this?

** Because it feels good.

** It feels good to spend money.

** It feels good to abstract code.

** It feels good to be clever.

* In conclusion
