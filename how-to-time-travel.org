#+REVEAL_PLUGINS: (highlight notes)
#+EXPORT_FILE_NAME: public/testing-from-scratch
#+OPTIONS: reveal_title_slide:nil
#+OPTIONS: toc:nil
#+OPTIONS: num:nil

* Intro to Reset Hard

* How time travel works

* My needs

- no unnecessary complexity
- low abstractions
- be truthful

* Source code overview

* Challenges 

* APIs and functional design

* Memory Usage and Leaks

* CPU usage (waves)

* Upcoming efforts

* Getting rid of the garbage collector

* Data compression
